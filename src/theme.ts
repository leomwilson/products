import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
    palette: {
      primary: {
        main: '#415a77',
        light: '#778da9',
        dark: '#1b263b',
        contrastText: '#e0e1dd'
      },
      secondary: {
        main: '#1b263b',
        light: '#415a77',
        dark: '#0d1b2a',
        contrastText: '#778da9'
      }
    },
  });