import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { createStyles, makeStyles, Theme, ThemeProvider } from '@material-ui/core/styles';
import { theme } from '../../theme';
import { Footer } from '../Footer';
import { Header } from '../Header';
import { Home } from '../Home';

export function App() {
  const classes = makeStyles((theme: Theme) =>
      createStyles({
        root: {
          flexGrow: 1
        },
        body: {
          padding: 5
        }
      })
  )();

  return (
    <div className={classes.root}>
      <ThemeProvider theme={theme}>
        <header><Header /></header>
        <div className={classes.body}>
          <br /><br />
          <BrowserRouter>
            <Switch>
              <Route path="/" component={Home} />
            </Switch>
          </BrowserRouter>
          <br /><br />
        </div>
        <footer><Footer /></footer>
      </ThemeProvider>
    </div>
  );
}

export default App;
