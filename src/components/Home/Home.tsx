import React from "react";
import { About } from "./About";
import { FAQ } from "./FAQ";
import { ProductList } from "./ProductList";

export function Home() {
    return (
        <div>
            <ProductList />
            <About />
            <FAQ />
        </div>
    );
}