import React from "react";
import { Typography, Link } from "@material-ui/core";

export function About() {
    return (
        <div>
            <Typography variant="h4" align="center">About Us</Typography>
            <Typography variant="body1">
                APL was founded in 2020 by <Link href="https://leomwilson.com" color="inherit">Leo Wilson</Link>.
                Our (I say "our" as if I, Leo Wilson, am not the only employee)
                goal is to provide the best trinkets at the best prices.
                Our products are born out of a frustration with the market's offerings.
                Our competitors were all either poorly designed or expensive,
                so we created alternatives that are both well-designed and affordable.
            </Typography>
        </div>
    );
}