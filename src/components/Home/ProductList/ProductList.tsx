import React, { ReactNode } from "react";
import { Typography, Grid, makeStyles, Theme, createStyles, ButtonBase, Button } from "@material-ui/core";
import { Link } from "react-router-dom";
import Rick from "../../../assets/img/products/rick.jpg";

interface ProductEntryProps {
    name: string,
    page: string,
    img: string, // image should be 1:1 for best results
    children: ReactNode // description
}

function ProductEntry(props: ProductEntryProps) {
    const classes = makeStyles((theme: Theme) =>
      createStyles({
        image: {
          width: 128,
          height: 128
        },
        img: {
          margin: 'auto',
          display: 'block',
          maxWidth: '100%',
          maxHeight: '100%'
        },
        paper: {
            width: '100%'
        }
      }),
    )();

    return (
        <Grid container spacing={2} direction="row">
            <Grid item>
                <ButtonBase className={classes.image}>
                    <Link to={props.page}>
                        <img className={classes.img} alt={props.name} src={props.img} />
                    </Link>
                </ButtonBase>
            </Grid>
            <Grid item>
                <Typography variant="h6" color="inherit">{props.name}</Typography>
                <Typography variant="body1" color="inherit">{props.children}</Typography>
                <br />
                <Link to={props.page}>
                    <Button variant="outlined" size="small">
                        More Info
                    </Button>
                </Link>
            </Grid>
        </Grid>
    );
}

export function ProductList() {
    return (
        <div>
            <Typography variant="h4" align="center">Products</Typography>
            <ProductEntry name="ID Holder" page="/idh" img={Rick}>
                Test description
            </ProductEntry>
        </div>
    );
}