import React from "react";
import { Typography } from "@material-ui/core";

export function FAQ() {
    return (
        <div>
            <Typography variant="h4" align="center">FAQ</Typography>
            <Typography variant="h6">How do you create your products?</Typography>
            <Typography variant="body1">
                The process varies product-to-product, but most use some form of 3D-printing.
                They are designed in Fusion 360 and printed on a Prusa i3 mk3.
            </Typography>
            <Typography variant="h6">Why don't you offer X?</Typography>
            <Typography variant="body1">
                More than likely, we don't care enough about it.
                One of our strengths as a company and as a brand is that we use everything we make.
                If we wouldn't use it ourselves, we wouldn't be passionate enough about the product
                to put in the effort and attention to detail usually characteristic of our work.
                Of course, you're always welcome to contact us and suggest something.
                There are things that we'd like to create but don't due to a perceived lack of interest,
                and showing your support might motivate us to change our minds.
                Do note that we generally don't make custom items.
                We aren't a design company and we aren't an on-demand 3D-printing company.
            </Typography>
            <Typography variant="h6">How did you create this website?</Typography>
            <Typography variant="body1">
                With React, Material UI, TypeScript, VSCode, and suffering.
            </Typography>
            <Typography variant="h6">How did you create your logo?</Typography>
            <Typography variant="body1">
                I drew it with coordinates and equations on Desmos,
                then I converted those into an image
                by manually editing an SVG file in Vim.
                It was fun.
            </Typography>
            <Typography variant="h6">I have another question.</Typography>
            <Typography variant="body1">
                Contact us! It might even make it on to this page.
            </Typography>
        </div>
    );
}