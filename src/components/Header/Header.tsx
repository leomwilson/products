import React from "react";
import { AppBar, Toolbar, IconButton, Typography, Link } from "@material-ui/core";
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import EmailIcon from '@material-ui/icons/Email';
import { Logo } from "../Logo";
import { Mastodon } from '../Logo/Mastodon';

export function Header() {
    const classes = makeStyles((theme: Theme) =>
      createStyles({
        menuButton: {
          marginRight: theme.spacing(2),
        },
        title: {
          flexGrow: 1,
        },
      }),
    )();
    return (
        <AppBar position="static">
            <Toolbar>
              <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                <Link href="/" color="inherit"><Logo fontSize="large" /></Link>
              </IconButton>
              <Typography variant="h6" color="inherit" className={classes.title}>
                Assorted Products by Leo
              </Typography>
              <IconButton color="inherit" aria-label="follow us on mastodon">
                <Link href="https://mastodon.social/@apl" color="inherit"><Mastodon /></Link>
              </IconButton>
              <IconButton color="inherit" aria-label="email us">
                <Link href="mailto:products@leomwilson.com" color="inherit"><EmailIcon /></Link>
              </IconButton>
            </Toolbar>
          </AppBar>
    );
}