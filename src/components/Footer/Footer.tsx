import React from "react";
import { Typography, Link } from "@material-ui/core";

function year(): string {
    const y = new Date().getFullYear();
    return (y === 2020) ? y.toString() : ('2020-' + y);
}

export function Footer() {
    return (
        <Typography variant="body2" align="center" color="textSecondary">
            &copy; {year()} <Link href="https://products.leomwilson.com" color="inherit">APL</Link>.
            This website is <Link href="https://gitlab.com/leomwilson/products" color="inherit">open source</Link>.
        </Typography>
    );
}