import React from "react";
import { SvgIcon, SvgIconProps } from "@material-ui/core";

export function Logo(props: SvgIconProps) {
    // 24x24
    // dilated by 1.5 to fit
    // translated down by 3 to centre
    return (
        <SvgIcon {...props}>
            <polygon points="10.5,3 10.5,21 13.5,21 13.5,3" id="centre" />
            <polygon points="9,3 0,21 3,21 9,9" id="a" />
            <path id="p" d="M 18 3 H 15 V 6 H 18
                A 3 1.5 0 0 1 18 9
                H 15 V 12 H 18
                A 6 4.5 0 0 0 18 3" />
            <polygon points="15,21 15,18 24,18 24,21" id="l" />
        </SvgIcon>
    )
}